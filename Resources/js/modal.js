
'use strict';

$(document).ready(function()
{
    $('#modal-delete-item').on('show.bs.modal', function (e) {

        let link = $(e.relatedTarget);

        $(this).find('.modal-title, .modal-body p').each(function () {
            let fix = $(this).html().replace('%name%', link.data('name'));
            $(this).html(fix)
        });

        $(this).find('#modal-confirm-action').on('click', function () {
            document.location.href = link.attr('href');
        });
    });
});
