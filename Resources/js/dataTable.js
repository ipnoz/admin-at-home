
'use strict';

if ('undefined' !== typeof($.fn.dataTable)) {

    let storagePrefix = 'admin@home_DataTables_';
    let storageFirstLetter = storagePrefix+'firstLetterFilter';

    let administrationEmailTableId = '#administration-email-table';
    let firstLetterInputId = '#filter-first-letters-input';

    /*
     * Filter a table by matching the first letters of the first table row
     */
    function filterByFirstLetters(settings, data, dataIndex)
    {
        let input = $(firstLetterInputId).val();

        if ('' === input) {
            return true;
        }

        let row0 = data[0];
        let regex = new RegExp('^'+input+'', 'gi');

        return regex.test(row0);
    }

    $.fn.dataTable.ext.search.push(filterByFirstLetters);

    $(firstLetterInputId).val(sessionStorage.getItem(storageFirstLetter));

    $(document).ready(function()
    {
        let administrationEmailTable = $(administrationEmailTableId).DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            columnDefs: [
                {orderable: false, targets: 5},
                {orderData: [5, 0], targets: 0}
            ],
            stateSave: true,
            colReorder: true,
            stateSaveCallback: function(settings,data) {
                sessionStorage.setItem(storagePrefix + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function(settings) {
                return JSON.parse(sessionStorage.getItem(storagePrefix + settings.sInstance));
            },
            dom: '<"dataTables_toolbar"l<"dataTables_clear_button">f>rtip',
            initComplete: function(){
                $('div.dataTables_clear_button')
                    .html('<button id="clear-datatables-state" class="btn btn-primary" type="reset">Clear</button>');
            }
        });

        // Enabling the filter first letter input
        $(firstLetterInputId).on('keyup', function() {
            administrationEmailTable.draw();
            sessionStorage.setItem(storageFirstLetter, $(firstLetterInputId).val());
        });

        // Reseting order & filters of the table
        $('#clear-datatables-state').on('click', function () {
            $(firstLetterInputId).val('');
            sessionStorage.removeItem(storageFirstLetter);
            administrationEmailTable.search('').columns().search('').draw();
            administrationEmailTable.order([[5, 'asc'], [0, 'asc']]).draw();
        });
    });
}
