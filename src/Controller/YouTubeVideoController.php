<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\YoutubeChannel;
use App\Entity\YoutubeVideo;
use App\Form\Type\PostButtonHideVideoType;
use App\Form\Type\PostButtonType;
use App\Repository\YoutubeVideoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/youtube_video", name="app_youtube_video_")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class YouTubeVideoController extends AbstractAppController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/hide/{id}", name="hide", methods={"POST"})
     */
    public function hideAction(YoutubeVideo $youtubeVideo, YoutubeVideoRepository $videoRepository, Request $request): Response
    {
        $form = $this->createForm(PostButtonHideVideoType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $youtubeVideo->hide();
            $channel = $youtubeVideo->getChannel();
            if ($channel instanceof YoutubeChannel) {
                $activeVideos = $videoRepository->findActiveByChannel($channel);
                if (empty($activeVideos) || (\count($activeVideos) === 1 && $activeVideos[0] === $youtubeVideo)) {
                    $channel->setAppearedAt(null);
                }
            }
            $this->entityManager->flush();

            if ($request->isXmlHttpRequest()) {
                return $this->render('Pages/Common/_hidden_video_item.html.twig', [
                    'video' => $youtubeVideo,
                    YoutubeActivitiesController::TWIG_DISPLAY_CHANNEL_NAME => $form->get(PostButtonHideVideoType::DISPLAY_CHANNEL_NAME)->getData(),
                ]);
            }
            return $this->redirectToReferer($request, 'app_youtube_channels_activities');
        }

        if ($request->isXmlHttpRequest()) {
            return new Response($this->getFormErrorMessage($form), Response::HTTP_PRECONDITION_FAILED);
        }

        $this->addFlashOnFormError($form);
        return $this->redirectToReferer($request, 'app_youtube_channels_activities');
    }

    /**
     * @Route("/remove_hide/{id}", name="remove_hide", methods={"POST"})
     */
    public function removeHideAction(YoutubeVideo $youtubeVideo, Request $request): RedirectResponse
    {
        $form = $this->createForm(PostButtonType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $youtubeVideo->unhide();
            $this->entityManager->flush();
        }

        $this->addFlashOnFormError($form);
        return $this->redirectToReferer($request, 'app_youtube_channels_activities');
    }
}
