<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\YoutubeChannel;
use App\Form\YoutubeChannelAddType;
use App\Form\YoutubeChannelEditType;
use App\Manager\YoutubeChannelManager;
use App\Repository\YoutubeChannelRepository;
use App\Repository\YoutubeVideoRepository;
use App\Service\YoutubeChannelActivities\ChannelActivitiesService;
use App\Service\YoutubeChannelActivities\ChannelModel;
use App\Service\YoutubeChannelActivities\FindAllVideosOfChannelService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/youtube_channel", name="app_youtube_channel_")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class YoutubeChannelController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private YoutubeChannelManager $manager;
    private YoutubeChannelRepository $repository;

    public function __construct(EntityManagerInterface $entityManager, YoutubeChannelManager $manager, YoutubeChannelRepository $repository)
    {
        $this->entityManager = $entityManager;
        $this->manager = $manager;
        $this->repository = $repository;
    }

    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function indexAction(): Response
    {
        $youtubeChannels = $this->repository->findBy([], ['name' => 'ASC']);
        return $this->render('Pages/YoutubeChannel/index.html.twig', [
            'channels' => $youtubeChannels,
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, FindAllVideosOfChannelService $findAllVideosService): Response
    {
        $youtubeChannel = new YoutubeChannel();

        $form = $this->createForm(YoutubeChannelAddType::class, $youtubeChannel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($youtubeChannel);

            if ($form->get(YoutubeChannelAddType::FETCH_ALL_VIDEOS_FIELD)->getData()) {
                $findAllVideosService->fetchAndPersist($youtubeChannel);
                $this->addFlash('success', 'La chaine Youtube a été créée et toutes ses vidéos importées');
            } else {
                $this->addFlash('success', 'La chaine Youtube a été créée');
            }

            return $this->redirectToRoute('app_youtube_channel_edit', ['id' => $youtubeChannel->getId()]);
        }

        return $this->render('Pages/YoutubeChannel/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, YoutubeChannel $youtubeChannel): Response
    {
        $form = $this->createForm(YoutubeChannelEditType::class, $youtubeChannel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($youtubeChannel);
            return $this->redirectToRoute($request->get('_route'), ['id' => $youtubeChannel->getId()]);
        }

        return $this->render('Pages/YoutubeChannel/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete", methods={"GET"})
     */
    public function deleteCommandAction(YoutubeChannel $youtubeChannel, CacheItemPoolInterface $cacheItemPool): RedirectResponse
    {
        $cacheKey = ChannelActivitiesService::CACHE_ITEM_PREFIX.$youtubeChannel->getId();

        $this->entityManager->remove($youtubeChannel);
        $this->entityManager->flush();

        try {
            $cacheItemPool->deleteItem($cacheKey);
        } catch (InvalidArgumentException $e) {}

        $this->addFlash('success', 'La chaine Youtube "'.$youtubeChannel->getName().'" a été supprimée (id: '.$youtubeChannel->getId().' )');

        return $this->redirectToRoute('app_youtube_channel_index');
    }

    /**
     * @Route("/show/{id}", name="show", methods={"GET"}, requirements={"id"=".+"})
     */
    public function show(YoutubeChannel $youtubeChannel, YoutubeVideoRepository $videoRepository): Response
    {
        $model = ChannelModel::fromEntity($youtubeChannel);
        $model->stats = $videoRepository->countNotHiddenAndAllByChannel($youtubeChannel->getId());

        return $this->render('Pages/YoutubeChannel/show.html.twig', [
            'channel' => $model,
            'active_videos' => $videoRepository->findActiveByChannel($youtubeChannel),
            'hidden_videos' => $videoRepository->findHiddenByChannel($youtubeChannel),
        ]);
    }
}
