<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\EmailAliasFilter;
use App\Form\EmailAliasFilterType;
use App\Repository\EmailAliasFilterRepository;
use App\Service\AdministrationEmail\CreateAliasFilterService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/administration/email/filter_aliases", name="app_administration_email_filter_aliases_")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class EmailAliasFilterController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function indexAction(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $aliasFilters = $em->getRepository(EmailAliasFilter::class)->findAll();

        return $this->render('Pages/AdministrationEmails/filter-aliases.html.twig', [
            'aliasFilters' => $aliasFilters
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, CreateAliasFilterService $service): Response
    {
        $data = new EmailAliasFilter();

        $form = $this->createForm(EmailAliasFilterType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var EmailAliasFilter $data */
            $data = $form->getData();

            $id = $service->createAndSave($data->getFilter(), $data->getComment(), $data->getFromURL());
            $this->addFlash('success', 'Le filtre a été créé');

            return $this->redirectToRoute('app_administration_email_filter_aliases_edit', ['id' => $id]);
        }

        return $this->render('Pages/AdministrationEmails/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, EmailAliasFilter $aliasFilter, EmailAliasFilterRepository $repository): Response
    {
        $form = $this->createForm(EmailAliasFilterType::class, $aliasFilter, ['route_action' => 'edit']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $repository->save($aliasFilter);
            return $this->redirectToRoute($request->get('_route'), ['id' => $aliasFilter->getId()]);
        }

        return $this->render('Pages/AdministrationEmails/form.html.twig', [
            'form' => $form->createView(),
            'aliasFilter' => $aliasFilter
        ]);
    }

    /**
     * @Route("/command/trash/{id}", name="command_trash", methods={"GET"})
     */
    public function moveToTrashCommandAction(EmailAliasFilter $aliasFilter): RedirectResponse
    {
        $aliasFilter->setIsEnabled(false);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('app_administration_email_filter_aliases_index');
    }

    /**
     * @Route("/command/untrash/{id}", name="command_untrash", methods={"GET"})
     */
    public function removeFromTrashCommandAction(EmailAliasFilter $aliasFilter): RedirectResponse
    {
        $aliasFilter->setIsEnabled(true);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('app_administration_email_filter_aliases_index');
    }

    /**
     * @Route("/command/delete/{id}", name="command_delete", methods={"GET"})
     */
    public function deleteCommandAction(EmailAliasFilter $aliasFilter): RedirectResponse
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($aliasFilter);
        $em->flush();

        return $this->redirectToRoute('app_administration_email_filter_aliases_index');
    }
}
