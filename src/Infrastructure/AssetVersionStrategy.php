<?php

declare(strict_types=1);

namespace App\Infrastructure;

use Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Asset version based on the md5 of the file contents
 */
final class AssetVersionStrategy implements VersionStrategyInterface
{
    private ParameterBagInterface $parameters;

    public function __construct(ParameterBagInterface $parameters)
    {
        $this->parameters = $parameters;
    }

    public function getVersion($path): string
    {
        return \md5_file($this->getRealpath($path));
    }

    public function applyVersion($path): string
    {
        return $path.'?'.$this->getVersion($path);
    }

    /*
     * Get the real asset path in any situation
     */
    private function getRealpath(string $path): string
    {
        $root = $this->parameters->get('kernel.project_dir');
        return \realpath($root.'/public/'.$path);
    }
}
