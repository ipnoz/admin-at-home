<?php

declare(strict_types=1);

namespace App\Infrastructure\KnpMenu;

use App\Repository\YoutubeChannelRepository;
use App\Repository\YoutubeVideoRepository;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class SideMenuBuilder
{
    public const ACCORDION_ID = 'accordion_id';

    private RequestStack $requestStack;
    private FactoryInterface $factory;
    private RouterInterface $router;
    private YoutubeChannelRepository $youtubeChannelRepository;
    private YoutubeVideoRepository $youtubeVideoRepository;
    private ParameterBagInterface $parameterBag;
    private TranslatorInterface $translator;

    public function __construct(
        RequestStack $requestStack,
        FactoryInterface $factory,
        RouterInterface $router,
        YoutubeChannelRepository $youtubeChannelRepository,
        YoutubeVideoRepository $youtubeVideoRepositor,
        ParameterBagInterface $parameterBag,
        TranslatorInterface $translator
    ) {
        $this->requestStack = $requestStack;
        $this->factory = $factory;
        $this->router = $router;
        $this->youtubeChannelRepository = $youtubeChannelRepository;
        $this->youtubeVideoRepository = $youtubeVideoRepositor;
        $this->parameterBag = $parameterBag;
        $this->translator = $translator;
    }

    public function build(): ItemInterface
    {
        $currentRoute = $this->getCurrentRoute();
        $currentUri = $this->getRequestUri();

        $menu = $this->factory->createItem('side_bar', [
            'label' => $this->parameterBag->get('app.website_title'),
        ]);
        $menu->setExtra(self::ACCORDION_ID, 'accordionSidebar');
        $this->addIconFa($menu, 'fas fa-laugh-wink');

        $this->addHeading($menu, 'Interfaces');

        $emails = $menu->addChild('emails', [
            'label' => 'Emails',
            'uri' => $this->router->generate('app_administration_email_filter_aliases_index'),
            'current' => ($currentRoute === 'app_administration_email_filter_aliases_index'),
        ]);
        $this->addIconFa($emails, 'fas fa-fw fa-mail-bulk');

        $youtubeActivity = $menu->addChild('youTube_activity', [
            'label' => 'Activitées YouTube',
            'uri' => $this->router->generate('app_youtube_channels_activities'),
            'current' => ($currentRoute === 'app_youtube_channels_activities'),
        ]);
        $this->addIconFa($youtubeActivity, 'fab fa-fw fa-youtube');

        $this->createChannelsFavoritesMenu($menu);

        $menuParameters = $this->addChildMenu($menu, 'parameters', 'Paramètres');
        $this->addIconFa($menuParameters, 'fas fa-fw fa-folder');

        $appParameters = $menuParameters->addChild('app_parameters', [
            'label' => 'Paramètres',
            'uri' => $this->router->generate('app_parameters'),
            'current' => ($currentRoute === 'app_parameters'),
        ]);
        $this->addIconFa($appParameters, 'fas fa-fw fa-sliders-h');

        $youTubeChannels = $menuParameters->addChild('youtube_channels', [
            'label' => 'Chaines YouTube',
            'uri' => $this->router->generate('app_youtube_channel_index'),
            'current' => \in_array($currentRoute,
                [
                    'app_youtube_channel_index',
                    'app_youtube_channel_new',
                    'app_youtube_channel_edit'
                ]),
        ]);
        $this->addIconFa($youTubeChannels, 'fab fa-fw fa-youtube');

        $this->addHeading($menu, 'Composants');

        $url = $this->router->generate('app_homepage').'phpMyAdmin/';
        $phpMyAdmin = $menu->addChild('php_my_admin', [
            'label' => 'phpMyAdmin',
            'uri' => $url,
            'current' => ($currentUri === $url),
        ]);
        $this->addIconFa($phpMyAdmin, 'fas fa-fw fa-wrench');

        $url = $this->router->generate('app_homepage').'phpMumbleAdmin/';
        $phpMumbleAdmin = $menu->addChild('php_mumble_admin', [
            'label' => 'phpMumbleAdmin (v0.4.3)',
            'uri' => $url,
            'current' => ($currentUri === $url),
        ]);
        $this->addIconFa($phpMumbleAdmin, 'fas fa-fw fa-wrench');

        $url = $this->router->generate('app_homepage').'backups/';
        $backup = $menu->addChild('backup', [
            'label' => 'Backups',
            'uri' => $url,
            'current' => ($currentUri === $url),
        ]);
        $this->addIconFa($backup, 'fas fa-fw fa-archive');

        $this->addDivider($menu);

        $menuOther = $this->addChildMenu($menu, 'other', 'Other pages');
        $this->addIconFa($menuOther, 'fas fa-fw fa-folder');

        $url = $this->router->generate('app_homepage').'mailq.php';
        $menuOther->addChild('mailq', [
            'label' => 'Mailq',
            'uri' => $url,
            'current' => ($currentUri === $url),
        ]);

        $url = 'http://192.168.0.2';
        $menuOther->addChild('wifi_router', [
            'label' => 'WIFI access point (routeur wifi)',
            'uri' => $url,
            'current' => ($currentRoute === $url),
        ]);

        $url = $this->router->generate('app_homepage').'phpinfo.php';
        $menuOther->addChild('php_info', [
            'label' => 'PHPinfo()',
            'uri' => $url,
            'current' => ($currentUri === $url),
        ]);

        $url = $this->router->generate('app_homepage').'server_info';
        $menuOther->addChild('server_info', [
            'label' => '/server_info',
            'uri' => $url,
            'current' => ($currentUri === $url),
        ]);

        $this->addDivider($menu);

        return $menu;
    }

    private function getCurrentRoute(): string
    {
        $request = $this->requestStack->getCurrentRequest();

        if (! $request instanceof Request) {
            return '';
        }

        return $request->get('_route', '');
    }

    private function getRequestUri(): string
    {
        $request = $this->requestStack->getCurrentRequest();

        if (! $request instanceof Request) {
            return '';
        }

        return $request->getRequestUri();
    }

    private function addHeading(ItemInterface $item, string $name): void
    {
        $heading = $item->addChild($name);
        $heading->setExtra('heading', $name);
    }

    private function addDivider(ItemInterface $item): void
    {
        $divider = $item->addChild('divider_'.\uniqid());
        $divider->setExtra('divider', true);
    }

    private function addChildMenu(ItemInterface $item, string $name, string $label): ItemInterface
    {
        $child = $item->addChild($name, [
            'label' => $label,
        ]);
        $child->setExtra(self::ACCORDION_ID, $name.'_'.\uniqid());
        return $child;
    }

    private function addIconFa(ItemInterface $item, string $name): void
    {
        $item->setExtra('fa_icon', $name);
    }

    private function createChannelsFavoritesMenu(ItemInterface $item): void
    {
        $channels = $this->youtubeChannelRepository->findFavorites();

        if (empty($channels)) {
            return;
        }

        $currentUri = $this->getRequestUri();

        $menuChannelList = $this->addChildMenu($item, 'channel_list', $this->translator->trans('sidemenu.favorites.title', [], 'sidemenu'));
        $this->addIconFa($menuChannelList, 'fas fa-fw fa-folder');

        foreach ($channels as $channel) {
            $stats = $this->youtubeVideoRepository->countNotHiddenByChannel($channel->getId());
            $url = $this->router->generate('app_youtube_channel_show', ['id' => $channel->getId()]);
            $menuChannelList->addChild($channel->getId(), [
                'label' => $this->translator->trans('sidemenu.favorites.channel.name', [
                    'channel_name' => $channel->getName(),
                    'not_hidden' => $stats->getTotalNotHiddenVideos(),
                    ], 'sidemenu'),
                'uri' => $url,
                'current' => ($url === $currentUri),
            ]);
        }
    }
}
