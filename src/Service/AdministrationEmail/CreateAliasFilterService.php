<?php

declare(strict_types=1);

namespace App\Service\AdministrationEmail;

use App\Entity\EmailAliasFilter;
use App\Repository\EmailAliasFilterRepository;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class CreateAliasFilterService
{
    private EmailAliasFilterRepository $repository;
    private string $aliasFilterDomain;
    private string $aliasFilterForward;

    public function __construct(
        EmailAliasFilterRepository $repository,
        string $aliasFilterDomain,
        string $aliasFilterForward
    ) {
        $this->repository = $repository;
        $this->aliasFilterDomain = $aliasFilterDomain;
        $this->aliasFilterForward = $aliasFilterForward;
    }

    public function createEntity(string $filter, ?string $comment = null, ?string $fromUrl = null): EmailAliasFilter
    {
        $aliasFilter = new EmailAliasFilter();

        $aliasFilter->setEmail($filter.$this->aliasFilterDomain);
        $aliasFilter->setForward($this->aliasFilterForward);
        $aliasFilter->setComment($comment);
        $aliasFilter->setFromURL($fromUrl);
        $aliasFilter->setIsEnabled(true);

        return $aliasFilter;
    }

    public function doesFilterNameExist(string $filter): bool
    {
        $result = $this->repository->findOneBy(['email' => $filter.$this->aliasFilterDomain]);
        return ($result instanceof EmailAliasFilter);
    }

    public function createAndSave(string $filter, ?string $comment = null, ?string $fromUrl = null): int
    {
        $aliasFilter = $this->createEntity($filter, $comment, $fromUrl);
        return $this->repository->save($aliasFilter);
    }
}
