<?php

declare(strict_types=1);

namespace App\Service\HttpClient;

use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class Response
{
    private ResponseInterface $response;

    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    public function getContent(): string
    {
        return $this->response->getContent();
    }
}
