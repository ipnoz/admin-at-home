<?php

declare(strict_types=1);

namespace App\Service\HttpClient;

use Symfony\Component\HttpClient\ScopingHttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use function str_replace;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class HttpClient
{
    private HttpClientInterface $httpClient;
    private array $youtubeParameters;
    private array $errors = [];

    public function __construct(HttpClientInterface $httpClient, array $youtubeParameters)
    {
        $this->httpClient = ScopingHttpClient::forBaseUri($httpClient, $youtubeParameters['api_url_base']);
        $this->youtubeParameters = $youtubeParameters;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function getErrorMessages(): array
    {
        $apiKey = $this->youtubeParameters['developer_key'];

        $messages = [];
        foreach ($this->getErrors() as $exception) {
            if ($exception instanceof ClientExceptionInterface) {
                $response = $exception->getResponse();
                $message = $response->toArray(false)['error']['message'];
            } else {
                $message = $exception->getMessage();
            }
            // Removing the api key from error messages
            $messages[] = str_replace($apiKey, '--OBFUSCATED_API_KEY--', $message);
        }

        return $messages;
    }

    public function request(string $method, string $url, array $options = []): Response
    {
        $response = $this->httpClient->request($method, $url, $options);
        return new Response($response);
    }

    /**
     * Get a list of responses contents in a row
     *
     * @param Response[] $requests
     *
     * @return array
     */
    public function getResponsesContents(array $responses, bool $getTranformed = false): array
    {
        $contents = [];

        foreach ($responses as $key => $response) {
            try {
                $contents[$key] = $response->getContent();
            } catch (\Exception $e) {
                $this->errors[$key] = $e;
            }
        }

        if ($getTranformed) {
            $contents = HttpClientUtil::transformResponsesContentsToObjects($contents);
        }

        return $contents;
    }
}
