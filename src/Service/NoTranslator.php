<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Contracts\Translation\LocaleAwareInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class NoTranslator implements TranslatorInterface, LocaleAwareInterface
{
    public function trans(string $id, array $parameters = [], string $domain = null, string $locale = null): string
    {
        return self::noTransWithParameters($id, $parameters);
    }

    public static function noTransWithParameters(string $id, array $parameters = []): string
    {
        $return = $id;

        if (! empty($parameters)) {
            $return .= ' '.\json_encode($parameters);
        }

        return $return;
    }

    /**
     * No op
     * {@inheritDoc}
     */
    public function getLocale(): string
    {
        return '';
    }

    /**
     * No op
     * {@inheritDoc}
     */
    public function setLocale(string $locale): void { }
}
