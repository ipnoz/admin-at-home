<?php

declare(strict_types=1);

namespace App\Service\YoutubeChannelActivities;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ChannelStats
{
    private int $totalOfVideos = 0;
    private int $totalNotHiddenVideos = 0;

    public function getTotalOfVideos(): int
    {
        return $this->totalOfVideos;
    }

    public function setTotalOfVideos(int $totalOfVideos): void
    {
        $this->totalOfVideos = $totalOfVideos;
    }

    public function getTotalNotHiddenVideos(): int
    {
        return $this->totalNotHiddenVideos;
    }

    public function setTotalNotHiddenVideos(int $totalNotHiddenVideos): void
    {
        $this->totalNotHiddenVideos = $totalNotHiddenVideos;
    }
}
