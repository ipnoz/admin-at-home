<?php

declare(strict_types=1);

namespace App\Service\YoutubeChannelActivities;

use App\Entity\YoutubeChannel;
use App\Entity\YoutubeVideo;
use App\Service\HttpClient\HttpClient;
use App\Util\DateUtil;
use Doctrine\ORM\EntityManagerInterface;
use function array_chunk;
use function array_merge;
use function http_build_query;
use function is_string;

/**
 * YouTube API documentation for Search::list
 * https://developers.google.com/youtube/v3/docs/search/list
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class FindAllVideosOfChannelService
{
    private HttpClient $httpClient;
    private EntityManagerInterface $entityManager;
    private array $parameters;

    public function __construct(HttpClient $httpClient, EntityManagerInterface $entityManager, array $youtubeParameters)
    {
        $this->httpClient = $httpClient;
        $this->entityManager = $entityManager;
        $this->parameters = $youtubeParameters;
    }

    public function fetchAndPersist(YoutubeChannel $youtubeChannel): void
    {
        $uploadPlaylistId = $this->fetchUploadPlaylistId($youtubeChannel);

        if (! $uploadPlaylistId) {
            return;
        }

        $items = $this->fetchAllVideosOfUploadPlaylist($uploadPlaylistId);

        $youtubeVideos = [];
        foreach ($items as $item) {
            $youtubeVideo = Util::hydrateYoutubeVideoWithContentResponse($item, $youtubeChannel);
            $this->entityManager->persist($youtubeVideo);
            $youtubeVideos[] = $youtubeVideo;
        }

        $parts = array_chunk($youtubeVideos, Constant::YOUTUBE_MAX_RESULT);
        foreach ($parts as $part) {
            $this->fetchAndSetDurationOfVideos($part);
        }

        $this->entityManager->flush();
    }

    // YouTube API quota: 1
    private function fetchUploadPlaylistId(YoutubeChannel $youtubeChannel): ?string
    {
        $queryParameters = [
            'key' => $this->parameters['developer_key'],
            'id' => $youtubeChannel->getId(),
            'part' => 'id,contentDetails',
        ];

        $response = $this->httpClient->request('GET', 'channels?'.http_build_query($queryParameters));
        $contents = $this->httpClient->getResponsesContents([$response], true);
        Assert::arrayContainsOnlyYouTubeApiResponse($contents, 'channelList');

        if (
            ! isset($contents[0]->items[0]->contentDetails->relatedPlaylists->uploads)
            || $contents[0]->items[0]->id !== $youtubeChannel->getId()
        ) {
            return null;
        }

        return $contents[0]->items[0]->contentDetails->relatedPlaylists->uploads;
    }

    /**
     * YouTube API quota: 1 per page
     *
     * @return object[]
     */
    private function fetchAllVideosOfUploadPlaylist(string $uploadPlaylistId, string $pageToken = null): array
    {
        // Find channel upload playlist items (YouTube api quota = 1)
        $queryParameters = [
            'key' => $this->parameters['developer_key'],
            'playlistId' => $uploadPlaylistId,
            'maxResults' => Constant::YOUTUBE_MAX_RESULT,
            'part' => 'snippet',
        ];
        if (is_string($pageToken)) {
            $queryParameters['pageToken'] = $pageToken;
        }

        $response = $this->httpClient->request('GET', 'playlistItems?'.http_build_query($queryParameters));
        $contents = $this->httpClient->getResponsesContents([$response], true);
        Assert::arrayContainsOnlyYouTubeApiResponse($contents, 'playlistItemList');

        if (! isset($contents[0])) {
            return [];
        }

        $content = $contents[0];
        $items = $content->items;

        if (isset($content->nextPageToken)) {
            $items = array_merge($items, $this->fetchAllVideosOfUploadPlaylist($uploadPlaylistId, $content->nextPageToken));
        }

        return $items;
    }

    /**
     * YouTube API quota: 1 per page
     *
     * @param YoutubeVideo[] $youtubeVideos
     */
    private function fetchAndSetDurationOfVideos(array $youtubeVideos): void
    {
        // Avoid invalid request parameter from YouTube api
        Assert::arrayIsMaxYouTubeIds($youtubeVideos);

        $lastKey = \count($youtubeVideos) -1;
        $ids = '';
        foreach ($youtubeVideos as $key => $youtubeVideo) {
            $ids .= $youtubeVideo->getId();
            if ($key !== $lastKey) {
                $ids .= ',';
            }
        }

        // Find channel upload playlist items (YouTube api quota = 1)
        $queryParameters = [
            'key' => $this->parameters['developer_key'],
            'id' => $ids,
            'maxResults' => Constant::YOUTUBE_MAX_RESULT,
            'part' => 'contentDetails',
        ];

        $response = $this->httpClient->request('GET', 'videos?'.http_build_query($queryParameters));
        $contents = $this->httpClient->getResponsesContents([$response], true);
        Assert::arrayContainsOnlyYouTubeApiResponse($contents, 'videoList');

        if (! isset($contents[0])) {
            return;
        }

        $items = $contents[0]->items;

        foreach ($youtubeVideos as $youtubeVideo) {
            foreach ($items as $item) {
                if ($item->id === $youtubeVideo->getId() && \is_string($item->contentDetails->duration)) {
                    $youtubeVideo->setDuration(DateUtil::formatDuration($item->contentDetails->duration));
                    break;
                }
            }
        }
    }
}
