<?php

declare(strict_types=1);

namespace App\Service\YoutubeChannelActivities;

use App\Service\HttpClient\HttpClient;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Contracts\Cache\ItemInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class YouTubeChannelService
{
    private HttpClient $httpClient;
    private CacheItemPoolInterface $cacheItemPool;
    private array $parameters;

    public function __construct(
        HttpClient $httpClient,
        CacheItemPoolInterface $cacheItemPool,
        array $youtubeParameters
    ) {
        $this->httpClient = $httpClient;
        $this->cacheItemPool = $cacheItemPool;
        $this->parameters = $youtubeParameters;
    }

    public function channelIdExist(string $id): bool
    {
        $contents = $this->fetchChannelData($id);

        if (! isset($contents[0]) || $contents[0]->pageInfo->totalResults === 0) {
            return false;
        }

        if ($contents[0]->pageInfo->totalResults === 1) {
            return true;
        }

        return false;
    }

    public function getChannelName(string $id): string
    {
        $contents = $this->fetchChannelData($id);

        if (! isset($contents[0], $contents[0]->items[0])) {
            return '';
        }

        return $contents[0]->items[0]->snippet->title;
    }

    private function fetchChannelData(string $id): array
    {
        return $this->cacheItemPool->get($id, function(ItemInterface $item) use ($id) {

            $item->expiresAfter(3600);

            $query = \http_build_query([
                'key' => $this->parameters['developer_key'],
                'id' => $id,
                'part' => 'snippet',
            ]);

            $response = $this->httpClient->request('GET', 'channels?'.$query);
            $contents = $this->httpClient->getResponsesContents([$response], true);
            Assert::arrayContainsOnlyYouTubeApiResponse($contents, 'channelList');

            return $contents;
        });
    }
}
