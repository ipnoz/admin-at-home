<?php

declare(strict_types=1);

namespace App\Service\YoutubeChannelActivities;

use App\Entity\YoutubeChannel;
use App\Entity\YoutubeVideo;
use DateTime;
use DateTimeImmutable;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ChannelModel
{
    public string $id;
    public string $title;
    public string $url;
    public DateTime $createdAt;
    public ?DateTimeImmutable $apparitionAt;
    /** @var YoutubeVideo[] */
    public array $activities = [];
    public ChannelStats $stats;

    public function __construct(string $title, string $id)
    {
        $this->id = $id;
        $this->title = $title;
        $this->url = Util::getChannelUrl($id);
        $this->stats = new ChannelStats();
    }

    public static function fromEntity(YoutubeChannel $channel): self
    {
        $model = new self($channel->getName(), $channel->getId());
        $model->createdAt = clone $channel->getCreatedAt();
        $model->apparitionAt = $channel->getAppearedAt();
        return $model;
    }

    public function addVideoItem(YoutubeVideo $item): void
    {
        $this->activities[] = $item;
    }
}
