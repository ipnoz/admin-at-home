<?php

declare(strict_types=1);

namespace App\Service\YoutubeChannelActivities;

use RuntimeException;
use function is_object;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class Assert
{
    public static function isYouTubeApiResponse($item, string $api): bool
    {
        return is_object($item) && isset($item->kind) && $item->kind === 'youtube#'.$api.'Response';
    }

    /**
     * Helper for array
     *
     * @throws RuntimeException
     */
    public static function arrayContainsOnlyYouTubeApiResponse(array $list, string $api): void
    {
        foreach ($list as $item) {
            if (! self::isYouTubeApiResponse($item, $api)) {
                throw new RuntimeException('Invalid content response received - it must be a Youtube::'.$api.' json response transformed in object');
            }
        }
    }

    public static function arrayIsMaxYouTubeIds(array $ids): void
    {
        if (\count($ids) > Constant::YOUTUBE_MAX_RESULT) {
            throw new \InvalidArgumentException('Too much videos ids. Set the limit to '.Constant::YOUTUBE_MAX_RESULT);
        }
    }
}
