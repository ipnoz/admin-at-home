<?php

declare(strict_types=1);

namespace App\Validator\Constraints;

use App\Service\YoutubeChannelActivities\YouTubeChannelService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class YouTubeChannelIdValidator extends ConstraintValidator
{
    private YouTubeChannelService $service;

    public function __construct(YouTubeChannelService $service)
    {
        $this->service = $service;
    }


    public function validate($value, Constraint $constraint)
    {
        if (! $constraint instanceof YouTubeChannelId) {
            throw new UnexpectedTypeException($constraint, YouTubeChannelId::class);
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) to take care of that
        if (null === $value || '' === $value) {
            return;
        }

        if (! \is_string($value)) {
            throw new \UnexpectedValueException($value, 'string');
        }

        if (! $this->service->channelIdExist($value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('channel_id', $value)
                ->setTranslationDomain('validation')
                ->addViolation();
        }
    }
}
