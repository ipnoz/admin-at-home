<?php

declare(strict_types=1);

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class AliasFilter extends Constraint
{
    public string $message = '"{{ string }}" filter already exists.';
}


