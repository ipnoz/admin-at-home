<?php

declare(strict_types=1);

namespace App\Validator\Constraints;

use App\Service\AdministrationEmail\CreateAliasFilterService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class AliasFilterValidator extends ConstraintValidator
{
    private CreateAliasFilterService $service;

    public function __construct(CreateAliasFilterService $service)
    {
        $this->service = $service;
    }

    public function validate($value, Constraint $constraint)
    {
        if (! $constraint instanceof AliasFilter) {
            throw new UnexpectedTypeException($constraint, AliasFilter::class);
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) to take care of that
        if (null === $value || '' === $value) {
            return;
        }

        if (! is_string($value)) {
            throw new \UnexpectedValueException($value, 'string');
        }

        if ($this->service->doesFilterNameExist($value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }
}