<?php

declare(strict_types=1);

namespace App\Util;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DateUtil
{
    static function formatDuration(string $duration): string
    {
        $dateInterval = new \DateInterval($duration);

        if ((int) $dateInterval->format('%h') > 0) {
            $return = $dateInterval->format('%h:%i:%S');
        } else {
            $return = $dateInterval->format('%i:%S');
        }

        return $return;
    }
}
