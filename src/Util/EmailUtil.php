<?php

declare(strict_types=1);

namespace App\Util;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class EmailUtil
{
    static function emailToMaildir(string $email): string
    {
        if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \RuntimeException('Invalid email received');
        }

        [$user, $domain] = explode('@', $email);

        return $domain.'/'.$user;
    }
}
