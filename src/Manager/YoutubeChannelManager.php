<?php

declare(strict_types=1);

namespace App\Manager;

use App\Entity\YoutubeChannel;
use App\Entity\YoutubeVideo;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class YoutubeChannelManager
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(YoutubeChannel $entity): void
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    /**
     * Set AppearedAt with the first video of channel found
     *
     * @param YoutubeVideo[] $videoList
     */
    public function updateAppearedAt(array $videoList): void
    {
        $flush = false;

        foreach ($videoList as $video) {
            // Set AppearedAt with the first video of channel found
            if ($video->getChannel()->getAppearedAt() === null) {
                $flush = true;
                $video->getChannel()->setAppearedAt(\DateTimeImmutable::createFromMutable($video->getPublishedAt()));
            }
        }

        if ($flush) {
            $this->entityManager->flush();
        }
    }
}
