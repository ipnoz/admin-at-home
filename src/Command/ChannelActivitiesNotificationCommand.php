<?php

declare(strict_types=1);

namespace App\Command;

use App\Repository\YoutubeChannelRepository;
use App\Service\MailerService;
use App\Service\YoutubeChannelActivities\ChannelActivitiesService;
use App\Service\YoutubeChannelActivities\Util;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ChannelActivitiesNotificationCommand extends Command
{
    protected static $defaultName = 'app:notification:new-youtube-video';

    private MailerService $mailer;
    private YoutubeChannelRepository $channelRepository;
    private ChannelActivitiesService $channelActivitiesService;
    private TranslatorInterface $translator;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        MailerService $mailer,
        YoutubeChannelRepository $channelRepository,
        ChannelActivitiesService $channelActivitiesService,
        TranslatorInterface $translator
    ) {
        $this->mailer = $mailer;
        $this->channelRepository = $channelRepository;
        $this->channelActivitiesService = $channelActivitiesService;
        $this->translator = $translator;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Send a new video uploaded notification by email')
            ->setHelp('This command send a notification by email that a new video has been uploaded in Youtube')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $channels = $this->channelRepository->findNotOffline();
        $newVideos = $this->channelActivitiesService->fetchNewVideos($channels);
        $channels = Util::aggregateVideosPerChannel($newVideos);

        foreach ($channels as $channel) {
            $message = $this->translator->trans('notification.youtube_activity.command_uploaded_video', [
                'channel_name' => $channel->title,
                'count' => \count($channel->activities)],
                'notification'
            );
            $output->writeln($message);
            $this->mailer->notifyYouTubeChannelHasUploadedVideos($channel);
        }

        return 0;
    }
}
