<?php

declare(strict_types=1);

namespace App\Repository;

use App\Componant\Constant\Status;
use App\Entity\YoutubeChannel;
use App\Entity\YoutubeVideo;
use App\Service\YoutubeChannelActivities\ChannelStats;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method YoutubeVideo|null find($id, $lockMode = null, $lockVersion = null)
 * @method YoutubeVideo|null findOneBy(array $criteria, array $orderBy = null)
 * @method YoutubeVideo[]    findAll()
 * @method YoutubeVideo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class YoutubeVideoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, YoutubeVideo::class);
    }

    /**
     * @param YoutubeVideo[] $youtubeVideos
     */
    public function saveList(array $youtubeVideos): void
    {
        $em = $this->getEntityManager();

        foreach ($youtubeVideos as $youtubeVideo) {
            $em->persist($youtubeVideo);
        }

        $em->flush();
    }

    public function findActiveQueryBuilder(): QueryBuilder
    {
        $qb = $this->createQueryBuilder('entity');

        $qb
            ->leftJoin('entity.channel', 'channel')
            ->where($qb->expr()->isNull('entity.hiddenAt'))
            ->orderBy('entity.publishedAt', 'DESC')
        ;

        return $qb;
    }

    /**
     * @return YoutubeVideo[]
     */
    public function findActiveOrderByPublishedAt(): array
    {
        $qb = $this->findActiveQueryBuilder();
        $qb
            ->andWhere($qb->expr()->eq('channel.status', ':channel_status'))
            ->setParameter('channel_status', Status::ONLINE)
        ;
        return $qb->getQuery()->getResult();
    }

    /**
     * @return  YoutubeVideo[]
     */
    public function findActiveByChannel(YoutubeChannel $youtubeChannel): array
    {
        $qb = $this->findActiveQueryBuilder();
        $this->andWhereByChannel($qb, $youtubeChannel);
        return $qb->getQuery()->getResult();
    }

    /**
     * @throws \Exception
     */
    public function findHiddenQueryBuilder(?int $latestDays = null): QueryBuilder
    {
        $qb = $this->createQueryBuilder('entity');

        $qb
            ->leftJoin('entity.channel', 'channel')
            ->where($qb->expr()->isNotNull('entity.hiddenAt'))
            ->orderBy('entity.hiddenAt', 'DESC')
        ;

        if (\is_int($latestDays)) {
            $latestDays = new \DateTimeImmutable('-'.$latestDays.' days');
            $qb
                ->andWhere($qb->expr()->gte('entity.hiddenAt', ':latest_days'))
                ->setParameter('latest_days', $latestDays->format('Y-m-d H:i:s'))
            ;
        }

        return $qb;
    }

    /**
     * @return  YoutubeVideo[]
     * @throws \Exception
     */
    public function findHidden(int $latestDays = null): array
    {
        $qb = $this->findHiddenQueryBuilder($latestDays);
        $qb
            ->andWhere($qb->expr()->eq('channel.status', ':channel_status'))
            ->setParameter('channel_status', Status::ONLINE)
        ;
        return $qb->getQuery()->getResult();
    }

    /**
     * @return  YoutubeVideo[]
     * @throws \Exception
     */
    public function findHiddenByChannel(YoutubeChannel $youtubeChannel): array
    {
        $qb = $this->findHiddenQueryBuilder();
        $this->andWhereByChannel($qb, $youtubeChannel);
        return $qb->getQuery()->getResult();
    }

    private function andWhereByChannel(QueryBuilder $qb, YoutubeChannel $youtubeChannel): void
    {
        $qb
            ->andWhere($qb->expr()->eq('channel.id', ':channel_id'))
            ->setParameter('channel_id', $youtubeChannel->getId())
        ;
    }

    public function countNotHiddenByChannel(string $channelId): ChannelStats
    {
        $notHidden = $this->count(['channel' => $channelId, 'hiddenAt' => null]);

        $stats = new ChannelStats();
        $stats->setTotalNotHiddenVideos($notHidden);

        return $stats;
    }

    public function countNotHiddenAndAllByChannel(string $channelId): ChannelStats
    {
        $total = $this->count(['channel' => $channelId]);
        $notHidden = $this->count(['channel' => $channelId, 'hiddenAt' => null]);

        $stats = new ChannelStats();
        $stats->setTotalOfVideos($total);
        $stats->setTotalNotHiddenVideos($notHidden);

        return $stats;
    }
}
