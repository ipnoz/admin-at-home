<?php

declare(strict_types=1);

namespace App\Repository;

use App\Componant\Constant\Status;
use App\Componant\Constant\YoutubeChannelStatus;
use App\Entity\YoutubeChannel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method YoutubeChannel|null find($id, $lockMode = null, $lockVersion = null)
 * @method YoutubeChannel|null findOneBy(array $criteria, array $orderBy = null)
 * @method YoutubeChannel[]    findAll()
 * @method YoutubeChannel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class YoutubeChannelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, YoutubeChannel::class);
    }

    public function findAllOnline(): array
    {
        return $this->findBy(['status' => Status::ONLINE]);
    }

    public function findFavorites(): array
    {
        return $this->findBy(['status' => YoutubeChannelStatus::FAVORITE]);
    }

    /**
     * @return YoutubeChannel[]
     */
    public function findNotOffline(): array
    {
        $qb = $this->createQueryBuilder('entity');
        $qb
            ->where($qb->expr()->neq('entity.status', ':status'))
            ->setParameter('status', Status::OFFLINE)
        ;
        return $qb->getQuery()->getResult();
    }
}
