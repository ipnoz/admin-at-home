<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Twig;

use App\Form\Type\PostButtonHideVideoType;
use App\Form\Type\PostButtonType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Routing\RouterInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class AppExtension extends AbstractExtension
{
    private FormFactoryInterface $formFactory;
    private RouterInterface $router;

    public function __construct(FormFactoryInterface $formFactory, RouterInterface $router)
    {
        $this->formFactory = $formFactory;
        $this->router = $router;
    }

    /**
     * {@inheritDoc}
     *
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('create_post_form', [$this, 'createPostForm']),
            new TwigFunction('create_post_form_hide_video', [$this, 'createPostFormHideVideo']),
        ];
    }

    public function createPostForm(string $action, array $parameters = []): FormView
    {
        $parameters['action'] = $action;
        return $this->formFactory->create(PostButtonType::class, null, $parameters)->createView();
    }

    public function createPostFormHideVideo(string $videoId, array $parameters = []): FormView
    {
        $parameters['action'] = $this->router->generate('app_youtube_video_hide', ['id' => $videoId]);
        return $this->formFactory->create(PostButtonHideVideoType::class, null, $parameters)->createView();
    }
}
