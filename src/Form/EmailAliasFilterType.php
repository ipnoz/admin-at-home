<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\EmailAliasFilter;
use App\Validator\Constraints\AliasFilter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class EmailAliasFilterType extends AbstractType
{
    const BLOCK_PREFIX = 'email_alias_filter';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ('new' === $options['route_action']) {
            $builder
                ->add('filter', null, [
                    'required' => true,
                    'label' => 'Filtre',
                    'constraints' => new AliasFilter()
                ])
            ;
        }

        if ('edit' === $options['route_action']) {
            $builder
                ->add('email', EmailType::class, ['label' => 'Filtre'])
                ->add('forward', EmailType::class, ['label' => 'Redirection'])
            ;
        }

        $builder
            ->add('comment', null, [
                'attr' => [
                    'rows' => 6
                ],
            ])
            ->add('fromURL', null, ['label' => 'Url d\'origine'])
            ->add('submit', SubmitType::class, [
                'label' => ('new' === $options['route_action']) ? 'Ajouter' : 'Modifier'
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EmailAliasFilter::class,
            'route_action' => 'new'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return self::BLOCK_PREFIX;
    }
}
