<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\YoutubeChannel;
use App\Form\Type\YoutubeChannelStatusType;
use App\Service\YoutubeChannelActivities\YouTubeChannelService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class YoutubeChannelAddType extends AbstractType
{
    const BLOCK_PREFIX = 'add_youtube_channel';
    const FETCH_ALL_VIDEOS_FIELD = 'fetch_all_videos';

    private YouTubeChannelService $service;

    public function __construct(YouTubeChannelService $youTubeChannelService)
    {
        $this->service = $youTubeChannelService;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', null, ['label' => 'Id de la chaine'])
            ->add('status', YoutubeChannelStatusType::class)
            ->add(self::FETCH_ALL_VIDEOS_FIELD, CheckboxType::class, [
                'mapped' => false,
                'required' => false,
                'label' => 'Récupérer toutes les vidéos',
            ])
            ->add('name', null, [
                'label' => 'Nom de la chaine',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, ['label' => 'Ajouter'])
        ;

        $this->getChannelNameIfEmptyEventListener($builder);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => YoutubeChannel::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return self::BLOCK_PREFIX;
    }

    private function getChannelNameIfEmptyEventListener(FormBuilderInterface $builder): void
    {
        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event): void {

            /** @var YoutubeChannel $data */
            $data = $event->getData();
            $form = $event->getForm();

            if (! $data || ! $form->isValid()) {
                return;
            }

            if ($data->getName() === null || \strlen($data->getName()) === 0) {
                $name = $this->service->getChannelName($data->getId());

                if (\strlen($name) === 0) {
                    $form->get('name')->addError(new FormError('Ce champs ne peut pas être vide'));
                } else {
                    $data->setName($name);
                }
            }
        });
    }
}
