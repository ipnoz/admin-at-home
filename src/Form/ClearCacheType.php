<?php

declare(strict_types=1);

namespace App\Form;

use App\Form\Type\PostButtonType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ClearCacheType extends AbstractType
{
    const BLOCK_PREFIX = 'clear_cache';

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'label' => 'Rafraichir le cache',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return self::BLOCK_PREFIX;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return PostButtonType::class;
    }
}
