<?php

declare(strict_types=1);

namespace App\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class YoutubeApiKeySubscriber implements EventSubscriberInterface
{
    private FlashBagInterface $flashBag;
    private array $youtubeParameters;

    public function __construct(FlashBagInterface $flashBag, array $youtubeParameters)
    {
        $this->flashBag = $flashBag;
        $this->youtubeParameters = $youtubeParameters;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => 'checkYouTubeApiKey',
        ];
    }

    public function checkYouTubeApiKey(): void
    {
        if (isset($this->youtubeParameters['developer_key']) && $this->youtubeParameters['developer_key'] !== 'this_is_not_a_real_private_key') {
            return;
        }

        $this->flashBag->add('danger', 'flash.youtube.api_key_not_defined');
    }
}
