<?php

declare(strict_types=1);

namespace App\Entity;

use App\Util\EmailUtil;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table()
 * @ORM\Entity()
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EmailMaildir extends Email
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    protected $password;

    /**
     * Non-persisted field that's used to create the encoded password.
     *
     * @var string
     */
    private $plainPassword;


    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = \password_hash($password, PASSWORD_BCRYPT, ['cost' => 13]);
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;
        /*
         * forces the object to look "dirty" to Doctrine. Avoids  Doctrine *not* saving this entity, if only
         * plainPassword changes
         */
        $this->password = null;
    }

    /*
     * Symfony calls this after logging in, and it's just a minor security measure to prevent the plain-text password
     * from being accidentally saved anywhere.
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /*
     * Custom helpers
     */

    public function getHome()
    {
        return EmailUtil::emailToMaildir(strToLower($this->getEmail()));
    }
}
