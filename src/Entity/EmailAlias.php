<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EmailAlias extends Email
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    protected $forward;


    public function getForward(): ?string
    {
        return $this->forward;
    }

    public function setForward(string $forward)
    {
        $this->forward = strToLower($forward);
    }
}
