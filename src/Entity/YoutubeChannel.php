<?php

declare(strict_types=1);

namespace App\Entity;

use App\Componant\Constant\Status;
use App\Validator\Constraints\YouTubeChannelId;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\Repository\YoutubeChannelRepository")
 * @UniqueEntity("id", message="La chaine Youtube est déjà inscrite dans la base de donnée")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class YoutubeChannel
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     * @ORM\Id
     * @Assert\NotBlank()
     * @YouTubeChannelId()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", options={"default"=Status::ONLINE})
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"EDIT"})
     */
    private $name;

    /**
     * @var ?\DateTimeImmutable
     *
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $appearedAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatedAt;


    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getAppearedAt(): ?\DateTimeImmutable
    {
        return $this->appearedAt;
    }

    public function setAppearedAt(?\DateTimeImmutable $appearedAt): void
    {
        $this->appearedAt = $appearedAt;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}
