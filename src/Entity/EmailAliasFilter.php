<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\Repository\EmailAliasFilterRepository")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EmailAliasFilter extends EmailAlias
{
    /**
     * Non-persisted field used to create the filter email.
     *
     * @var string
     */

    protected $filter;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=4000, nullable=true)
     */
    protected $fromURL;


    public function getFilter(): ?string
    {
        return $this->filter;
    }

    public function setFilter(string $filter): void
    {
        $this->filter = $filter;
    }

    public function getFromURL(): ?string
    {
        return $this->fromURL;
    }

    public function setFromURL(?string $fromURL): void
    {
        $this->fromURL = $fromURL;
    }
}
