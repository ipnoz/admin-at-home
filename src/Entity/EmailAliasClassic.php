<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table()
 * @ORM\Entity()
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EmailAliasClassic extends EmailAlias
{

}
