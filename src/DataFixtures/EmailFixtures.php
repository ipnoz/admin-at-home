<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class EmailFixtures extends Fixture
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        $faker->seed(1);

        /*
         * EmailMaildir
         */
        $domain = $faker->domainName;

        foreach (range(1, 4) as $id) {

            $user = $faker->firstName().'.'.$faker->lastName;
            $createdAt = $faker->dateTimeBetween('2013-01-09', '2020-11-05');
            $updatedAt = $faker->dateTimeBetween($createdAt->format('Y-m-d'), '2020-11-05');

            $maildir = new Entity\EmailMaildir();

            $maildir->setEmail($user.'@'.$domain);
            $maildir->setPassword($faker->password());
            $maildir->setCreatedAt($createdAt);
            if (in_array($id, [1, 3, 4])) {
                $maildir->setUpdatedAt($updatedAt);
            }
            $maildir->setIsEnabled(true);
            $maildir->setComment($faker->boolean() ? $faker->text() : '');

            $manager->persist($maildir);
        }

        /*
         * EmailAliasClassic
         */
        $domain = $faker->domainName;
        $forward = $faker->firstName().'@'.$domain;
        $y = 1;

        foreach (range(1, 26) as $id) {

            // Change the domain 3 times max
            if ($faker->boolean(25) && $y < 4) {
                $domain = $faker->domainName;
                $forward = $faker->firstName().'@'.$domain;
                ++$y;
            }

            $user = $faker->firstName().'.'.$faker->lastName;
            $createdAt = $faker->dateTimeBetween('2013-01-09', '2020-11-05');
            $updatedAt = $faker->dateTimeBetween($createdAt->format('Y-m-d'), '2020-11-05');

            $alias = new Entity\EmailAliasClassic();

            $alias->setEmail($user.'@'.$domain);
            $alias->setForward($forward);
            $alias->setCreatedAt($createdAt);
            if ($faker->boolean(63)) {
                $alias->setUpdatedAt($updatedAt);
            }
            $alias->setIsEnabled($isEnabled = $faker->boolean(90));
            $alias->setComment($comment = $faker->boolean(25) ? $faker->text() : '');

            $manager->persist($alias);
        }

        /*
         * EmailAliasFilter
         */
        $domain = $_ENV['FILTER_ALIAS_DOMAIN'];
        $forward = $_ENV['FILTER_ALIAS_FORWARD'];

        foreach (range(1, 181) as $id) {

            $createdAt = $faker->dateTimeBetween('2013-01-09', '2020-11-05');
            $updatedAt = $faker->dateTimeBetween($createdAt->format('Y-m-d'), '2020-11-05');

            $aliasFilter = new Entity\EmailAliasFilter();

            $aliasFilter->setEmail($faker->unique()->domainName.$domain);
            $aliasFilter->setForward($forward);
            $aliasFilter->setCreatedAt($createdAt);
            if ($faker->boolean(47)) {
                $aliasFilter->setUpdatedAt($updatedAt);
            }
            $aliasFilter->setIsEnabled($faker->boolean(90));
            $aliasFilter->setComment($faker->boolean(47) ? $faker->text($faker->numberBetween(100, 1500)) : '');
            $aliasFilter->setFromURL($faker->optional(0.35)->url);

            $manager->persist($aliasFilter);
        }

        $manager->flush();
    }
}
