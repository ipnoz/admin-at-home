<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Componant\Constant\Status;
use App\Entity\YoutubeChannel;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class YoutubeChannelFixtures extends Fixture
{
    private const CHANNELS = [
        'Fédération Française de Football' => 'UCeJlXGyEl7kBgQJKADAHM3A',
        'PSG - Paris Saint-Germain' => 'UCt9a_qP9CqHCNwilf-iULag',
        'beIN SPORTS France' => 'UCfj4kQ6_mYO5r4hzX5KloVw',
        'MotoGP' => 'UC8pYaQzbBBXg9GIOHRvTmDQ',
        'CANAL+ Sport' => 'UC8ggH3zU61XO0nMskSQwZdA',
        'BBC' => 'UCCj956IF62FbT7Gouszaj9w',
        'National Geographic Wild France' => 'UCT60XBtfRQzf5NKFGDNbfCw'
    ];

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        foreach (self::CHANNELS as $name => $id) {

            $youtubeChannel = new YoutubeChannel();

            $youtubeChannel->setName($name);
            $youtubeChannel->setId($id);
            $youtubeChannel->setStatus(('UC8pYaQzbBBXg9GIOHRvTmDQ' === $id) ? Status::OFFLINE : Status::ONLINE);

            $manager->persist($youtubeChannel);
        }

        $manager->flush();
    }
}
