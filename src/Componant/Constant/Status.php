<?php

declare(strict_types=1);

namespace App\Componant\Constant;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Status
{
    const ONLINE = 'online';
    const OFFLINE = 'offline';

    /**
     * @return string[]
     */
    public static function getStatutes(): array
    {
        return [
            self::ONLINE => self::ONLINE,
            self::OFFLINE => self::OFFLINE,
        ];
    }
}
