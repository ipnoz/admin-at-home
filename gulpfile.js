
let gulp = require('gulp');
let browserSync = require('browser-sync').create();
let del = require('del');
let sass = require('gulp-sass');
let cleanCSS = require('gulp-clean-css');
let rename = require("gulp-rename");
let concat = require('gulp-concat');
let uglify = require('gulp-uglify');

let SRC = './Resources';
let DEST = './public';
let TMP = './var/cache/dev/gulp';

let HTTP_PATH = '/var/www/html';
let START_PATH = __dirname.replace(HTTP_PATH, '')+'/'+DEST;

// css
function compileScss()
{
    return gulp.src(SRC+'/scss/**/*.scss')
        .pipe(sass.sync({outputStyle:'compressed'}).on('error', sass.logError))
        .pipe(cleanCSS({inline: 'none'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(DEST+'/css'))
        .pipe(browserSync.stream());
}

gulp.task('css', gulp.series(compileScss));

function concatJs()
{
    del([TMP+'/*']);
    return gulp.src(SRC + '/js/**/*.js')
        .pipe(concat('app.js'))
        .pipe(gulp.dest(TMP));
}

function minifyJs()
{
    return gulp.src(TMP + '/*.js')
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(DEST + '/js'));
}

gulp.task('js', gulp.series(concatJs, minifyJs));

function browserSyncDaemon() {
    browserSync.init({
        proxy: '172.18.0.1',
        open: false,
        notify: false,
        logFileChanges: false,
        reloadOnRestart: true,
        ghostMode: false,
        startPath: START_PATH,
    });
}

function watchFiles()
{
    gulp.watch(SRC+'/scss/**/*.scss', gulp.series('css'));
    gulp.watch(SRC+'/js/**/*.js', gulp.series('js'));
}

gulp.task('default', gulp.parallel(browserSyncDaemon, watchFiles));
