<?php

declare(strict_types=1);

namespace App\Tests\functional\Service\YoutubeChannelActivities;

use App\Entity\YoutubeVideo;
use App\Service\YoutubeChannelActivities\ChannelActivitiesService;
use App\Tests\FunctionalTester;
use App\Tests\Shared\Definition\YoutubeDefinitions;

final class ChannelActivitiesServiceCest
{
    public function it_test_get_parameter_method(FunctionalTester $I): void
    {
        /** @var ChannelActivitiesService $service */
        $service = $I->grabService(ChannelActivitiesService::class);
        $I->assertStringContainsString('http', $service->getParameter('api_url_base'));
    }

    public function it_test_get_parameter_method_with_unknown_key(FunctionalTester $I): void
    {
        /** @var ChannelActivitiesService $service */
        $service = $I->grabService(ChannelActivitiesService::class);
        $I->assertNull($service->getParameter('unknown_key'));
    }

    public function it_test_get_errors_method_return_empty_array(FunctionalTester $I): void
    {
        /** @var ChannelActivitiesService $service */
        $service = $I->grabService(ChannelActivitiesService::class);
        $I->assertSame([], $service->getHttpErrors());
    }

    public function it_fetch_new_videos_method_with_0_channels(FunctionalTester $I): void
    {
        /** @var ChannelActivitiesService $service */
        $service = $I->grabService(ChannelActivitiesService::class);

        $videosList = $service->fetchNewVideos([]);

        $I->assertSame([], $videosList);
    }

    public function it_test_constructor_setting_custom_published_after_days_parameter(FunctionalTester $I): void
    {
        $I->addParameterInRepository('youtube_activities_published_after_days', '33');

        /** @var ChannelActivitiesService $service */
        $service = $I->grabService(ChannelActivitiesService::class);

        $I->assertSame('33', $service->getParameter('published_after_days'));
    }

    public function it_test_that_the_service_throw_exception_when_it_receive_invalid_responses_from_youtube_api(FunctionalTester $I): void
    {
        $channel1 = $I->addYoutubeChannelInRepository(['id' => 'channel_with_invalid_response']);
        $I->startMockWebserver();
        $I->setupMockWebserverResponsesForChannels($I->getMockWebserverInstance(), ['channel_with_invalid_response' => 'invalid_response']);

        $I->expectThrowable(\RuntimeException::class, function() use ($I, $channel1) {
            /** @var ChannelActivitiesService $service */
            $service = $I->grabService(ChannelActivitiesService::class);
            $service->fetchNewVideos([$channel1]);
        });
    }

    public function it_fetch_new_videos_method_with_0_stored_video(FunctionalTester $I): void
    {
        $channels = [];
        foreach (YoutubeDefinitions::LIST as $channelId) {
            $channels[] = $I->addYoutubeChannelInRepository(['id' => $channelId]);
        }
        $I->startMockWebserver();
        $I->setupMockWebserverResponsesForChannels($I->getMockWebserverInstance(), YoutubeDefinitions::LIST);
        $I->setupMockWebserverResponsesForVideos($I->getMockWebserverInstance(), YoutubeDefinitions::VIDEO_LIST);

        /** @var ChannelActivitiesService $service */
        $service = $I->grabService(ChannelActivitiesService::class);
        $newVideos = $service->fetchNewVideos($channels);

        $I->assertCount(10, $newVideos);
        // the process have stored new videos in database
        $I->assertCount(10, $I->grabEntitiesFromRepository(YoutubeVideo::class));
    }

    public function it_fetch_new_videos_method_with_5_stored_videos(FunctionalTester $I): void
    {
        $channels = [];
        foreach (YoutubeDefinitions::LIST as $channelId) {
            $channels[] = $channels[$channelId] = $I->addYoutubeChannelInRepository(['id' => $channelId]);
        }
        $I->addYoutubeVideoInRepository(['id' => 'youtube-video-id-1', 'channel' => $channels['Channel-01-one_uploaded_video']]);
        $I->addYoutubeVideoInRepository(['id' => 'youtube-video-id-3', 'channel' => $channels['Channel-07-two_uploaded_videos']]);
        $I->addYoutubeVideoInRepository(['id' => 'youtube-video-id-5', 'channel' => $channels['Channel-10-three_uploaded_videos-one_user_subscription']]);
        $I->addYoutubeVideoInRepository(['id' => 'youtube-video-id-7', 'channel' => $channels['Channel-11-two_uploaded_videos-two_users_subscriptions-one_playlist']]);
        $I->addYoutubeVideoInRepository(['id' => 'youtube-video-id-9', 'channel' => $channels['Channel-15-two_uploaded_videos']]);

        $I->startMockWebserver();
        $I->setupMockWebserverResponsesForChannels($I->getMockWebserverInstance(), YoutubeDefinitions::LIST);
        $I->setupMockWebserverResponsesForVideos($I->getMockWebserverInstance(), ['youtube-video-id-2', 'youtube-video-id-4', 'youtube-video-id-6', 'youtube-video-id-8', 'youtube-video-id-10']);

        /** @var ChannelActivitiesService $service */
        $service = $I->grabService(ChannelActivitiesService::class);
        $channelActivities = $service->fetchNewVideos($channels);

        $I->assertCount(5, $channelActivities);
        // the process have stored new videos in database
        $I->assertCount(10, $I->grabEntitiesFromRepository(YoutubeVideo::class));
    }
}
