<?php

declare(strict_types=1);

namespace App\Tests\functional\Service;

use App\Entity\Parameter;
use App\Service\ParameterService;
use App\Tests\FunctionalTester;

class ParameterServiceCest
{
    public function it_test_service_adding_default_parameters_if_parameter_is_not_in_the_repository(FunctionalTester $I): void
    {
        $service = $I->grabService(ParameterService::class);
        $parameters = $service->getParameters();

        $I->assertCount(count(ParameterService::DEFAULT_PARAMETERS), $parameters);
        $I->assertContainsOnlyInstancesOf(Parameter::class, $parameters);
    }

    public function it_test_service_loading_parameters_from_repository(FunctionalTester $I): void
    {
        $storedParameters = [];
        foreach (ParameterService::DEFAULT_PARAMETERS as $key => $defaultValue) {
            $storedParameters[] = $I->addParameterInRepository($key, 'test-value-for-'.$key);
        }

        $service = $I->grabService(ParameterService::class);
        $parameters = $service->getParameters();

        $I->assertSame($storedParameters, $parameters);
    }

    public function it_test_service_dont_include_parameters_from_repository_if_not_in_the_default_parameters_enumerator(FunctionalTester $I): void
    {
        $storedParameter = $I->addParameterInRepository('key-is-not-in-the-default-parameters-enumerator', 'test-value');

        $service = $I->grabService(ParameterService::class);

        $I->assertNotContains($storedParameter, $service->getParameters());
    }

    public function it_test_has_method(FunctionalTester $I): void
    {
        $service = $I->grabService(ParameterService::class);

        $I->assertTrue($service->has(array_key_first(ParameterService::DEFAULT_PARAMETERS)));
        $I->assertFalse($service->has('unknown-key'));
    }

    public function it_test_update_and_save_parameters_method(FunctionalTester $I): void
    {
        foreach (ParameterService::DEFAULT_PARAMETERS as $key => $value) {
            $data[$key] = 'test-new-value-for-'.$key;
        }

        $service = $I->grabService(ParameterService::class);
        $service->updateAndSaveParameters($data);

        foreach (ParameterService::DEFAULT_PARAMETERS as $key => $value) {
            $I->seeInRepository(Parameter::class, ['id' => $key, 'value' => 'test-new-value-for-'.$key]);
        }
    }

    public function it_test_update_and_save_parameters_method_without_valid_keys(FunctionalTester $I): void
    {
        $service = $I->grabService(ParameterService::class);

        $service->updateAndSaveParameters([]);
        $service->updateAndSaveParameters(['invalid_key_id' => 'new-value']);

        foreach (ParameterService::DEFAULT_PARAMETERS as $key => $value) {
            $I->seeInRepository(Parameter::class, ['id' => $key, 'value' => $value]);
        }
    }

    public function it_test_update_and_save_parameters_method_remove_stored_parameter_if_is_not_in_the_default_parameters_enumerator(FunctionalTester $I): void
    {
        $storedParameter = $I->addParameterInRepository('key-is-not-in-the-default-parameters-enumerator', 'test-value');

        $service = $I->grabService(ParameterService::class);
        $service->updateAndSaveParameters([]);

        $I->assertNotContains($storedParameter, $I->grabEntitiesFromRepository(Parameter::class));
    }
}
