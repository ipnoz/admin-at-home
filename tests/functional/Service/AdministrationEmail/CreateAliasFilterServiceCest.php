<?php

declare(strict_types=1);

namespace App\Tests\functional\Service\AdministrationEmail;

use App\Entity\EmailAliasFilter;
use App\Service\AdministrationEmail\CreateAliasFilterService;
use App\Tests\FunctionalTester;

final class CreateAliasFilterServiceCest
{
    private CreateAliasFilterService $service;

    public function _before(FunctionalTester $I): void
    {
        $this->service = $I->grabService(CreateAliasFilterService::class);
    }

    public function it_create_and_save_an_alias_filter_in_database(FunctionalTester $I): void
    {
        $id = $this->service->createAndSave('filter-string-357');
        $I->seeInRepository(EmailAliasFilter::class, ['id' => $id]);
    }

    public function it_check_if_a_filter_name_exist_in_database(FunctionalTester $I): void
    {
        $I->addEmailAliasFilterInRepository(['email' => 'filter-string-411'.\getenv('FILTER_ALIAS_DOMAIN')]);

        $result = $this->service->doesFilterNameExist('filter-string-411');
        $I->assertTrue($result);
    }
}
