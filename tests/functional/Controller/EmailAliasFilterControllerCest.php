<?php

declare(strict_types=1);

namespace App\Tests\functional\Controller;

use App\Entity\EmailAliasFilter;
use App\Form\EmailAliasFilterType;
use App\Tests\FunctionalTester;

final class EmailAliasFilterControllerCest
{
    private const BASE_LOCATION = '/administration/email/filter_aliases';

    public function it_send_a_get_request_to_the_index_controler(FunctionalTester $I): void
    {
        $I->amOnPage(self::BASE_LOCATION.'/');
        $I->seeResponseCodeIsSuccessful();
    }

    public function it_send_a_get_request_to_the_new_alias_filter_controler(FunctionalTester $I): void
    {
        $I->stopFollowingRedirects();

        $I->amOnPage(self::BASE_LOCATION.'/new');
        $I->seeResponseCodeIsSuccessful();
    }

    public function it_send_a_post_request_to_the_new_alias_filter_controler(FunctionalTester $I): void
    {
        $I->stopFollowingRedirects();

        $I->sendPost(self::BASE_LOCATION.'/new', [
            EmailAliasFilterType::BLOCK_PREFIX => $newData = [
                'filter' => 'new-filter-field',
                'comment' => 'new-comment-field',
                'fromURL' => 'new-fromURL-field'
            ]
        ]);
        $I->seeResponseCodeIsRedirection();
    }

    public function it_send_a_get_request_to_the_edit_controler(FunctionalTester $I): void
    {
        $I->stopFollowingRedirects();

        $filterAlias = $I->addEmailAliasFilterInRepository();

        $I->amOnPage(self::BASE_LOCATION.'/edit/'.$filterAlias->getId());
        $I->seeResponseCodeIsSuccessful();
    }

    public function it_send_a_post_request_to_the_edit_controler(FunctionalTester $I): void
    {
        $I->stopFollowingRedirects();

        $filterAlias = $I->addEmailAliasFilterInRepository();

        $I->sendPost(self::BASE_LOCATION.'/edit/'.$filterAlias->getId(), [
            EmailAliasFilterType::BLOCK_PREFIX => $newData = [
                'email' => 'new-email-field',
                'forward' => 'new-forward-field',
                'comment' => 'new-comment-field',
                'fromURL' => 'new-fromURL-field'
            ]
        ]);
        $I->seeResponseCodeIsRedirection();
    }

    public function it_send_a_get_request_to_the_move_to_trash_command_controler(FunctionalTester $I): void
    {
        $I->stopFollowingRedirects();

        $filterAlias = $I->addEmailAliasFilterInRepository(['isEnabled' => true]);

        $I->amOnPage(self::BASE_LOCATION.'/command/trash/'.$filterAlias->getId());
        $I->seeResponseCodeIsRedirection();

        $I->seeInRepository(EmailAliasFilter::class, ['id' => $filterAlias->getId(), 'isEnabled' => false]);
    }

    public function it_send_a_get_request_to_the_remove_from_trash_command_controler(FunctionalTester $I): void
    {
        $I->stopFollowingRedirects();

        $filterAlias = $I->addEmailAliasFilterInRepository(['isEnabled' => false]);

        $I->amOnPage(self::BASE_LOCATION.'/command/untrash/'.$filterAlias->getId());
        $I->seeResponseCodeIsRedirection();

        $I->seeInRepository(EmailAliasFilter::class, ['id' => $filterAlias->getId(), 'isEnabled' => true]);
    }

    public function it_send_a_get_request_to_the_delete_command_controler(FunctionalTester $I): void
    {
        $I->stopFollowingRedirects();

        $filterAlias = $I->addEmailAliasFilterInRepository();

        $I->amOnPage(self::BASE_LOCATION.'/command/delete/'.$filterAlias->getId());
        $I->seeResponseCodeIsRedirection();

        $I->dontSeeInRepository(EmailAliasFilter::class, ['id' => $filterAlias->getId()]);
    }
}
