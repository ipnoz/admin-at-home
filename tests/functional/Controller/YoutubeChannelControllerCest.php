<?php

declare(strict_types=1);

namespace App\Tests\functional\Controller;

use App\Componant\Constant\Status;
use App\Entity\YoutubeChannel;
use App\Form\YoutubeChannelAddType;
use App\Form\YoutubeChannelEditType;
use App\Tests\FunctionalTester;

class YoutubeChannelControllerCest
{
    private const BASE_LOCATION = '/youtube_channel';

    public function it_send_a_get_request_to_the_index_page(FunctionalTester $I): void
    {
        $I->amOnPage(self::BASE_LOCATION.'/');
        $I->seeResponseCodeIsSuccessful();
    }

    public function it_send_a_get_request_to_the_new_page(FunctionalTester $I): void
    {
        $I->stopFollowingRedirects();

        $I->amOnPage(self::BASE_LOCATION.'/new');
        $I->seeResponseCodeIsSuccessful();
    }

    public function it_send_a_post_request_to_the_new_page(FunctionalTester $I): void
    {
        $I->sendPost(self::BASE_LOCATION.'/new', [
            YoutubeChannelAddType::BLOCK_PREFIX => [
                'id' => 'youtube-channel-id-field',
                'name' => 'youtube-channel-name-field'
            ]
        ]);
        $I->see('validation.youtube_channel_id.message {"channel_id":"youtube-channel-id-field"}');
    }

    public function it_send_a_get_request_to_the_edit_page(FunctionalTester $I): void
    {
        $I->stopFollowingRedirects();

        $youtubeChannel = $I->addYoutubeChannelInRepository();

        $I->amOnPage(self::BASE_LOCATION.'/edit/'.$youtubeChannel->getId());
        $I->seeResponseCodeIsSuccessful();
    }

    public function it_send_a_post_request_to_the_edit_page(FunctionalTester $I): void
    {
        $I->stopFollowingRedirects();

        $youtubeChannel = $I->addYoutubeChannelInRepository();

        $I->sendPost(self::BASE_LOCATION.'/edit/'.$youtubeChannel->getId(), [
            YoutubeChannelEditType::BLOCK_PREFIX => [
                'name' => 'youtube-channel-name-field',
                'status' => Status::OFFLINE,
            ]
        ]);
        $I->seeResponseCodeIsRedirection();
    }

    public function it_send_a_get_request_to_the_delete_page(FunctionalTester $I): void
    {
        $I->stopFollowingRedirects();

        $youtubeChannel = $I->addYoutubeChannelInRepository(['id' => 'youtube-channel-id-25']);

        $I->amOnPage(self::BASE_LOCATION.'/delete/'.$youtubeChannel->getId());
        $I->seeResponseCodeIsRedirection();
        $I->dontSeeInRepository(YoutubeChannel::class, ['id' => 'youtube-channel-id-25']);
    }
}
