<?php

declare(strict_types=1);

namespace App\Tests\functional\Command;

use App\Componant\Constant\Status;
use App\Componant\Constant\YoutubeChannelStatus;
use App\Entity\YoutubeChannel;
use App\Tests\Helper\Functional;
use App\Tests\Shared\Common\CreateEntities;
use App\Tests\Shared\Common\getJsonHttpContentResponse;
use App\Tests\Shared\Definition\YoutubeDefinitions;
use Doctrine\ORM\EntityManagerInterface;
use donatj\MockWebServer\MockWebServer;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

final class ChannelActivitiesNotificationCommandTest extends KernelTestCase
{
    use CreateEntities;
    use getJsonHttpContentResponse;

    protected static $kernel;
    private EntityManagerInterface $em;
    private MockWebServer $mockedWebServer;

    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        self::$kernel = static::bootKernel();

        $this->em = self::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->em->beginTransaction();
        $this->em->getConnection()->setAutoCommit(false);

        Functional::startMockWebserver();
        $this->mockedWebServer = Functional::getMockWebserverInstance();
    }

    /**
     * @inheritDoc
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        if ($this->em->getConnection()->isTransactionActive()) {
            $this->em->rollback();
        }
    }

    private function setupRepositoryAndMockWebserver(): void
    {
        $channels = YoutubeDefinitions::LIST;
        $videosIds = YoutubeDefinitions::VIDEO_LIST;
        $store = [];

        // Add all channels to repository
        foreach ($channels as $name => $id) {

            $status = ($id === YoutubeDefinitions::LIST['Channel-07']) ? YoutubeChannelStatus::FAVORITE : Status::ONLINE;

            $youtubeChannel = new YoutubeChannel();
            $youtubeChannel->setName($name);
            $youtubeChannel->setId($id);
            $youtubeChannel->setStatus($status);
            $youtubeChannel->setCreatedAt(new \DateTime());

            $store[$id] = $youtubeChannel;

            $this->em->persist($youtubeChannel);
        }

        // Setup all channels responses for the mocked WebServer
        $this->setupMockWebserverResponsesForChannels($this->mockedWebServer, $channels);

        // Add/store the youtube-video-id-5 video to the repository
        $channel10 = $store['Channel-10-three_uploaded_videos-one_user_subscription'];
        $youtubeVideo = $this->createYoutubeVideo(['id' => 'youtube-video-id-5', 'channel' => $channel10]);

        $this->em->persist($youtubeVideo);

        /*
         * The ChannelActivitiesService will request only for new videos
         * (not stored in the repository).
         * So the youtube-video-id-5 will not be requested by ChannelActivitiesService
         */
        unset($videosIds[5]);
        $this->setupMockWebserverResponsesForVideos($this->mockedWebServer, $videosIds);

        $this->em->flush();
    }

    /** @test */
    public function it_notify_by_email_channels_activities(): void
    {
        $this->setupRepositoryAndMockWebserver();

        $console = new Application(self::$kernel);
        $command = $console->find('app:notification:new-youtube-video');

        $commandTester = new CommandTester($command);
        $commandTester->execute(['command' => $command->getName()]);

        $this->assertSame(0, $commandTester->getStatusCode());
        $this->assertSame(
'notification.youtube_activity.command_uploaded_video {"channel_name":"Channel-01","count":1}
notification.youtube_activity.command_uploaded_video {"channel_name":"Channel-07","count":2}
notification.youtube_activity.command_uploaded_video {"channel_name":"Channel-10","count":2}
notification.youtube_activity.command_uploaded_video {"channel_name":"Channel-11","count":2}
notification.youtube_activity.command_uploaded_video {"channel_name":"Channel-15","count":2}
',
            $commandTester->getDisplay()
        );

        // Run again the command will find no new videos
        $commandTester->execute(['command' => $command->getName()]);
        $this->assertSame(0, $commandTester->getStatusCode());
        $this->assertSame('', $commandTester->getDisplay());
    }
}
