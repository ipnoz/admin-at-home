<?php

declare(strict_types=1);

namespace App\Tests\unit\Form;

use App\Entity\YoutubeChannel;
use App\Form\YoutubeChannelAddType;
use App\Service\YoutubeChannelActivities\YouTubeChannelService;
use Symfony\Component\Form\PreloadedExtension;
use Symfony\Component\Form\Test\TypeTestCase;

class YoutubeChannelAddTypeTest extends TypeTestCase
{
    protected function getExtensions(): array
    {
        $mockedYouTubeChannelService = $this->createMock(YouTubeChannelService::class);
        return [
            new PreloadedExtension([new YoutubeChannelAddType($mockedYouTubeChannelService)], []),
        ];
    }

    public function test_form_submission(): void
    {
        $formData = [
            'name' => 'Input name field',
            'id' => 'Input id field',
        ];

        $model = new YoutubeChannel();

        $form = $this->factory->create(YoutubeChannelAddType::class, $model);
        $form->submit($formData);

        $expected = new YoutubeChannel();
        $expected->setName($formData['name']);
        $expected->setId($formData['id']);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($expected, $model);
    }
}
