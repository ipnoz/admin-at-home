<?php

declare(strict_types=1);

namespace App\Tests\unit\Form;

use App\Entity\YoutubeChannel;
use App\Form\YoutubeChannelEditType;
use Symfony\Component\Form\Test\TypeTestCase;

class YoutubeChannelEditTypeTest extends TypeTestCase
{
    public function test_form_submission(): void
    {
        $formData = [
            'name' => 'Input name field',
        ];

        $model = new YoutubeChannel();

        $form = $this->factory->create(YoutubeChannelEditType::class, $model);
        $form->submit($formData);

        $expected = new YoutubeChannel();
        $expected->setName($formData['name']);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($expected, $model);
    }
}
