<?php

declare(strict_types=1);

namespace App\Tests\unit\Service\YoutubeChannelActivities;

use App\Service\YoutubeChannelActivities\ChannelModel;
use App\Tests\UnitTester;
use Codeception\Test\Unit;

final class ChannelModelTest extends Unit
{
    protected UnitTester $tester;

    public function test_constructor_setup_channel_title_parameter(): void
    {
        $youtubeChannelModel = new ChannelModel('channelTitle', 'channelId');
        $this->assertSame('channelTitle', $youtubeChannelModel->title);
    }

    public function test_constructor_setup_channel_id_parameter(): void
    {
        $youtubeChannelModel = new ChannelModel('channelTitle', 'channelId');
        $this->assertSame('channelId', $youtubeChannelModel->id);
    }

    public function test_constructor_setup_a_valid_url_parameter(): void
    {
        $youtubeChannelModel = new ChannelModel('channelTitle', 'channelId');
        $expected = filter_var($youtubeChannelModel->url, FILTER_VALIDATE_URL);

        $this->assertNotFalse($expected);
    }

    public function test_url_parameter_return_the_submitted_video_id(): void
    {
        $videolId = '2346~a5&62+69-aa=32=e65°b9)a1/f0|250(a0[9d]93}4bf{cc"05#5';
        $youtubeChannelModel = new ChannelModel('channelTitle', $videolId);

        $this->assertStringContainsString($videolId, $youtubeChannelModel->url);
    }

    public function test_activities_parameter_return_an_empty_array(): void
    {
        $youtubeChannelModel = new ChannelModel('channelTitle', 'channelId');
        $this->assertSame([], $youtubeChannelModel->activities);
    }

    public function test_add_video_item_method_increment_activities_parameter(): void
    {
        $youtubeChannelModel = new ChannelModel('channelTitle', 'channelId');
        $youtubeVideo = $this->tester->createYoutubeVideo();

        $youtubeChannelModel->addVideoItem($youtubeVideo);

        $this->assertSame([$youtubeVideo], $youtubeChannelModel->activities);
    }
}
