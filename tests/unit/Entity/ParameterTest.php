<?php

declare(strict_types=1);

namespace App\Tests\unit\Entity;

use App\Entity\Parameter;
use Codeception\Test\Unit;

final class ParameterTest extends Unit
{
    public function test_entity_invocation(): void
    {
        $this->assertInstanceOf(Parameter::class, new Parameter());
    }

    public function test_entity_setter_and_getter_for_key(): void
    {
        $YoutubeVideo = new Parameter();
        $YoutubeVideo->setId('string-key');

        $this->assertSame('string-key', $YoutubeVideo->getId());
    }

    public function test_entity_setter_and_getter_for_value(): void
    {
        $YoutubeVideo = new Parameter();
        $YoutubeVideo->setValue('string-value');

        $this->assertSame('string-value', $YoutubeVideo->getValue());
    }
}
