<?php

declare(strict_types=1);

namespace App\Tests\unit\Entity;

use App\Componant\Constant\Status;
use App\Entity\YoutubeChannel;
use Codeception\Test\Unit;

final class YoutubeChannelTest extends Unit
{
    public function test_entity_invocation(): void
    {
        $this->assertInstanceOf(YoutubeChannel::class, new YoutubeChannel());
    }

    public function test_entity_setter_and_getter_for_id(): void
    {
        $youtubeChannel = new YoutubeChannel();
        $youtubeChannel->setId('string-id');

        $this->assertSame('string-id', $youtubeChannel->getId());
    }

    public function test_entity_setter_and_getter_for_name(): void
    {
        $youtubeChannel = new YoutubeChannel();
        $youtubeChannel->setName('string-name-field');

        $this->assertSame('string-name-field', $youtubeChannel->getName());
    }

    public function test_entity_setter_and_getter_for_status(): void
    {
        $youtubeChannel = new YoutubeChannel();
        $youtubeChannel->setStatus(Status::OFFLINE);

        $this->assertSame(Status::OFFLINE, $youtubeChannel->getStatus());
    }

    public function test_entity_setter_and_getter_for_created_at(): void
    {
        $youtubeChannel = new YoutubeChannel();
        $youtubeChannel->setCreatedAt(new \DateTime('2018-07-15 18:45:00'));

        $this->assertSame('2018-07-15 18:45:00', $youtubeChannel->getCreatedAt()->format('Y-m-d H:i:s'));
    }

    public function test_entity_setter_and_getter_for_updated_at(): void
    {
        $youtubeChannel = new YoutubeChannel();
        $youtubeChannel->setCreatedAt(new \DateTime('2018-07-15 18:45:00'));

        $this->assertSame('2018-07-15 18:45:00', $youtubeChannel->getCreatedAt()->format('Y-m-d H:i:s'));
    }
}
