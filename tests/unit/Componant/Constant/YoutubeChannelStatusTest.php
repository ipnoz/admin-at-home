<?php

declare(strict_types=1);

namespace App\Tests\unit\Componant\Constant;

use App\Componant\Constant\Status;
use App\Componant\Constant\YoutubeChannelStatus;
use Codeception\Test\Unit;

final class YoutubeChannelStatusTest extends Unit
{
    public function test_get_statutes(): void
    {
        $result = YoutubeChannelStatus::getStatutes();

        $this->assertSame([
            Status::ONLINE => Status::ONLINE,
            Status::OFFLINE => Status::OFFLINE,
            YoutubeChannelStatus::FAVORITE => YoutubeChannelStatus::FAVORITE,
        ], $result);
    }
}
