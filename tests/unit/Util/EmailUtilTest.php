<?php

declare(strict_types=1);

namespace App\Tests\unit\Util;

use App\Util\EmailUtil;
use Codeception\Test\Unit;

final class EmailUtilTest extends Unit
{
    public function test_email_to_maildir_method(): void
    {
        $this->assertSame('domain.test.tld/test-foo.test.bar', EmailUtil::emailToMaildir('test-foo.test.bar@domain.test.tld'));
    }

    public function test_email_to_maildir_method_accept_only_valid_email(): void
    {
        $this->expectException(\RuntimeException::class);
        EmailUtil::emailToMaildir('test@no-tld');
    }
}
