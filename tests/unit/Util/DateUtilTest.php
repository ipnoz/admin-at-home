<?php

declare(strict_types=1);

namespace App\Tests\unit\Util;

use App\Util\DateUtil;
use Codeception\Test\Unit;

final class DateUtilTest extends Unit
{
    public function durationProvider(): array
    {
        return [
            '0:01' => ['PT1S', '0:01'],
            '0:35' => ['PT35S', '0:35'],
            '9:35' => ['PT9M35S', '9:35'],
            '29:35' => ['PT29M35S', '29:35'],
            '3:29:35' => ['PT3H29M35S', '3:29:35'],
            '13:29:35' => ['PT13H29M35S', '13:29:35']
        ];
    }

    /**
     * @dataProvider durationProvider
     */
    public function test_format_duration_method($duration, $expected): void
    {
        $this->assertSame($expected, DateUtil::formatDuration($duration));
    }
}
