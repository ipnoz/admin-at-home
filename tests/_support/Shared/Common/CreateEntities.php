<?php

declare(strict_types=1);

namespace App\Tests\Shared\Common;

use App\Componant\Constant\Status;
use App\Entity;
use App\Service\YoutubeChannelActivities\ChannelModel;

/*
 * Trait to create easily entities of the App.
 */
trait CreateEntities
{
    public function createChannelModel(array $data = []): ChannelModel
    {
        $default = [
            'title' => 'ChannelModel-title-field',
            'id' => 'ChannelModel-id-field',
        ];
        $data = array_merge($default, $data);

        $channelModel = new ChannelModel(
            $data['title'],
            $data['id']
        );

        return $channelModel;
    }

    public function createEmailAliasFilter(array $data = []): Entity\EmailAliasFilter
    {
        $default = [
            'email' => 'EmailAliasFilter-email-field',
            'forward' => 'EmailAliasFilter-forward-field',
            'comment' => 'EmailAliasFilter-comment-field',
            'createdAt' => new \DateTime(),
            'isEnabled' => true,
            'fromUrl' => 'EmailAliasFilter-fromUrl-field'
        ];
        $data = array_merge($default, $data);

        $aliasFilter = new Entity\EmailAliasFilter();

        $aliasFilter->setEmail($data['email']);
        $aliasFilter->setForward($data['forward']);
        $aliasFilter->setComment($data['comment']);
        $aliasFilter->setCreatedAt($data['createdAt']);
        $aliasFilter->setIsEnabled($data['isEnabled']);
        $aliasFilter->setFromURL($data['fromUrl']);

        return $aliasFilter;
    }

    public function createParameter($id, $value): Entity\Parameter
    {
        $parameter = new Entity\Parameter();
        $parameter->setId($id);
        $parameter->setValue($value);

        return $parameter;
    }

    public function createYoutubeChannel(array $data = []): Entity\YoutubeChannel
    {
        $default = [
            'id' => 'YoutubeChannel-id-field',
            'name' => 'YoutubeChannel-name-field',
            'status' => Status::ONLINE,
            'created_at' => new \DateTime('now'),
            'appeared_at' => null,
        ];
        $data = array_merge($default, $data);

        $youtubeChannel = new Entity\YoutubeChannel();

        $youtubeChannel->setId($data['id']);
        $youtubeChannel->setName($data['name']);
        $youtubeChannel->setStatus($data['status']);
        $youtubeChannel->setCreatedAt($data['created_at']);
        $youtubeChannel->setAppearedAt($data['appeared_at'] ? new \DateTimeImmutable($data['appeared_at']) : null);

        return $youtubeChannel;
    }

    public function createYoutubeVideo(array $data = []): Entity\YoutubeVideo
    {
        $default = [
            'id' => 'YoutubeVideo-id-field',
            'channel' => $this->createYoutubeChannel(),
            'name' => 'YoutubeVideo-name-field',
            'description' => 'YoutubeVideo-description-field',
            'duration' => '25:10',
            'thumbnails' => new \stdClass(),
            'url' => 'YoutubeVideo-url-field',
            'hiddenAt' => null,
            'published_at' => new \DateTime('-55 minutes')
        ];
        $data = array_merge($default, $data);

        $youtubeVideo = new Entity\YoutubeVideo();

        $youtubeVideo->setId($data['id']);
        $youtubeVideo->setChannel($data['channel']);
        $youtubeVideo->setName($data['name']);
        $youtubeVideo->setDescription($data['description']);
        $youtubeVideo->setDuration($data['duration']);
        $youtubeVideo->setThumbnails($data['thumbnails']);
        $youtubeVideo->setUrl($data['url']);
        $youtubeVideo->setHiddenAt($data['hiddenAt']);
        $youtubeVideo->setPublishedAt($data['published_at']);

        return $youtubeVideo;
    }
}
