<?php

declare(strict_types=1);

namespace App\Tests\Shared\Common;

/*
 * Trait to easily execute for tests private methods
 */

trait TestingPrivateMethod
{
    /**
     * @return mixed
     * @throws \ReflectionException
     */
    public function executePrivateMethod(object $class, string $methodName, array $args = [])
    {
        $reflection = new \ReflectionClass(get_class($class));

        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($class, $args);
    }
}
