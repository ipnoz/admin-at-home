<?php

declare(strict_types=1);

namespace App\Tests\Shared\Common;

use App\Service\HttpClient\HttpClientUtil;
use donatj\MockWebServer\MockWebServer;
use donatj\MockWebServer\Response;
use donatj\MockWebServer\ResponseStack;

trait getJsonHttpContentResponse
{
    public function getYoutubeActivitiesResponseForChannelId(string $channelId): string
    {
        $rootDir = __DIR__.'/../../../_data/YoutubeChannelActivitiesResponses';
        return file_get_contents($rootDir.'/'.$channelId.'.json');
    }

    public function getTransformedContentResponseForYoutubeVideoResources(array $ids): object
    {
        $rootDir = __DIR__.'/../../../_data/YoutubeVideoResourcesResponses';
        $response = file_get_contents($rootDir.'/response.json');
        $response = HttpClientUtil::transformContentResponseToObject($response);

        foreach ($ids as $id) {
            $item = file_get_contents($rootDir.'/'.$id.'.json');
            $response->items[] = HttpClientUtil::transformContentResponseToObject($item);
        }

        $response->totalResults = count($response->items);
        $response->resultsPerPage = count($response->items);

        return $response;
    }

    public function getTransformedContentResponseForChannelId(string $channelId): object
    {
        $contentResponse = $this->getYoutubeActivitiesResponseForChannelId($channelId);
        return HttpClientUtil::transformContentResponseToObject($contentResponse);
    }

    public function setupMockWebserverResponsesForChannels(MockWebServer $server, array $channels): void
    {
        $responses = [];

        foreach ($channels as $channelId) {
            $responses[] = new Response($this->getYoutubeActivitiesResponseForChannelId($channelId));
        }

        $server->setResponseOfPath('/youtube/v3/activities', new ResponseStack(...$responses));
    }

    public function setupMockWebserverResponsesForVideos(MockWebServer $server, array $videosIds): void
    {
        $videosResources = $this->getTransformedContentResponseForYoutubeVideoResources($videosIds);
        $videosResources = \json_encode($videosResources);

        $server->setResponseOfPath('/youtube/v3/videos', new Response($videosResources));
    }
}
