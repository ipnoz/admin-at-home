<?php

declare(strict_types=1);

namespace App\Tests\Shared\Definition;

final class YoutubeDefinitions
{
    const LIST = [
        'Channel-01' => 'Channel-01-one_uploaded_video',
        'Channel-02' => 'Channel-02-empty_activity_response',
        'Channel-03' => 'Channel-03-empty_activity_response',
        'Channel-04' => 'Channel-04-empty_activity_response',
        'Channel-05' => 'Channel-05-one_user-subscription',
        'Channel-06' => 'Channel-06-empty_activity_response',
        'Channel-07' => 'Channel-07-two_uploaded_videos',
        'Channel-08' => 'Channel-08-empty_activity_response',
        'Channel-09' => 'Channel-09-empty_activity_response',
        'Channel-10' => 'Channel-10-three_uploaded_videos-one_user_subscription',
        'Channel-11' => 'Channel-11-two_uploaded_videos-two_users_subscriptions-one_playlist',
        'Channel-12' => 'Channel-12-empty_activity_response',
        'Channel-13' => 'Channel-13-empty_activity_response',
        'Channel-14' => 'Channel-14-empty_activity_response',
        'Channel-15' => 'Channel-15-two_uploaded_videos'
    ];

    const VIDEO_LIST = [
        1 => 'youtube-video-id-1',
        2 => 'youtube-video-id-2',
        3 => 'youtube-video-id-3',
        4 => 'youtube-video-id-4',
        5 => 'youtube-video-id-5',
        6 => 'youtube-video-id-6',
        7 => 'youtube-video-id-7',
        8 => 'youtube-video-id-8',
        9 => 'youtube-video-id-9',
        10 => 'youtube-video-id-10'
    ];
}
