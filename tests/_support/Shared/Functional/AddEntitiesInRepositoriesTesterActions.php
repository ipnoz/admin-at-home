<?php

declare(strict_types=1);

namespace App\Tests\Shared\Functional;

use App\Entity;

trait AddEntitiesInRepositoriesTesterActions
{
    public function addEmailAliasFilterInRepository(array $data = []): Entity\EmailAliasFilter
    {
        $id = $this->haveInRepository($this->createEmailAliasFilter($data));
        return $this->grabEntityFromRepository(Entity\EmailAliasFilter::class, ['id' => $id]);
    }

    public function addParameterInRepository($id, $value): Entity\Parameter
    {
        $id = $this->haveInRepository($this->createParameter($id, $value));
        return $this->grabEntityFromRepository(Entity\Parameter::class, ['id' => $id]);
    }

    public function addYoutubeChannelInRepository(array $data = []): Entity\YoutubeChannel
    {
        $id = $this->haveInRepository($this->createYoutubeChannel($data));
        return $this->grabEntityFromRepository(Entity\YoutubeChannel::class, ['id' => $id]);
    }

    public function addYoutubeVideoInRepository(array $data = []): Entity\YoutubeVideo
    {
        $id = $this->haveInRepository($this->createYoutubeVideo($data));
        return $this->grabEntityFromRepository(Entity\YoutubeVideo::class, ['id' => $id]);
    }
}
